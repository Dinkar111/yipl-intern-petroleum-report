from prettytable import from_db_cursor
import json

import prettytable
from prettytable.prettytable import PrettyTable


class PetroleumProductClass:

    # Constructor Method
    def __init__(self, db):
        self.db = db

    # Create Table of Petroleum Product in db
    def create_table(self):
        self.db.execute("DROP TABLE IF EXISTS PETROLEUM_TABLE;")
        self.db.execute(''' 
            CREATE TABLE PETROLEUM_TABLE(
            ID INTEGER NOT NULL PRIMARY KEY,
            Year INT,
            Product VARCHAR(30),
            Sale INT,
            Country VARCHAR(30)
            );
        ''')
        print("Table has been created.")

    # Store the data in db
    def store_data_in_db(self, py_dict_data):
        arr_data = []

        for data in py_dict_data:
            arr_data.append((data["year"], data["petroleum_product"], data["sale"], data["country"]))

        self.db.executemany("INSERT INTO PETROLEUM_TABLE(Year, Product, Sale, Country) VALUES (?,?,?,?)", arr_data)
        print("Data has been Stored.")

    # get list of overall sales of each product by country
    def get_overall_sale_by_country(self):
        cursor_obj = self.db.execute("SELECT Country, Product, SUM(Sale) AS Overall_Sale FROM PETROLEUM_TABLE GROUP BY Country, Product")
        overall_sale_table = from_db_cursor(cursor_obj)
        cursor_obj.fetchall()
        print(overall_sale_table)

    # get list of avg sale of each product by 2 year interval
    def get_avg_sale_for_two_year(self):
        cursor_obj = self.db.execute("SELECT Product, Year, SUM(Sale) as Sum_Sale1 FROM PETROLEUM_TABLE GROUP BY Product, Year;")
        data = cursor_obj.fetchall()

        avg_sale_data = []

        for i in range(0,len(data)-1,2):
            product = data[i][0]
            year1 = data[i][1]
            year2 = data[i+1][1]
            sale1 = data[i][2]
            sale2 = data[i+1][2]
            if year2 == year1+1:
                avg_sale = (sale1 + sale2)/2
                year = str(year1)+"-"+str(year2)
                avg_sale_data.append([product, year, avg_sale])  
            else:
                None              

        avg_sale_table = PrettyTable()
        avg_sale_table.field_names = ["Product", "Year", "Average_Sale"]
        avg_sale_table.add_rows(avg_sale_data) 
        print(avg_sale_table)

    # least sale of each product at year
    def get_least_sale(self):
        cursor_obj = self.db.execute('''
            SELECT Product, Year, MIN(Sum_Sale) AS Min_Sale 
            FROM (
                SELECT Product, Year, SUM(Sale) AS Sum_Sale FROM PETROLEUM_TABLE GROUP BY Product, Year
            ) 
            WHERE Sum_Sale != 0 GROUP BY Product;
        ''')
        least_sale_table = from_db_cursor(cursor_obj)
        cursor_obj.fetchall()
        print(least_sale_table)    
        