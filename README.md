# Petroleum Product Report

## Create Virtual Environment using following command

python3 -m venv Environment_Folder_name

### To activate virtual env in terminal

source EnvironmentFolder/bin/activate

### To deactivate virtual env run command

deactivate

## Requirements.txt
Only one third party library has been used i.e. PrettyTable to print the data in form of table in CLI

### to install the packages used. run the following command

pip3 install -r requirements.txt

## To Run the program 

python3 report.py