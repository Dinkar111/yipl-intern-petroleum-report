import sqlite3
import urllib.request as request
import json

from reportgenerator import PetroleumProductClass

class DbClass:

    # Constructor Method
    def __init__(self, db_name):
        self.conn = sqlite3.connect(db_name)
        self.cur = self.conn.cursor()
        print( db_name+" Database is Connected" )

    # Method to execute db query
    def execute(self, dbquery):
        self.cur.execute(dbquery)
        self.conn.commit()
        return self.cur

    # Method to execute many
    def executemany(self, dbquery, data):
        self.cur.executemany(dbquery, data)
        self.conn.commit()
        return self.cur

class MainClass:

    # Constructer Method 
    def __init__(self):
        db_name = "petroleumData.db"
        self.db = DbClass(db_name)

    # Main Method
    def main(self):
        
        # Fetching data from url
        url = "https://raw.githubusercontent.com/younginnovations/internship-challenges/master/programming/petroleum-report/data.json"
        response_json_data = request.urlopen(url).read()
        py_dict_data = json.loads(response_json_data)

        # Initiate Petroleum Product Class
        petroleum_product_obj = PetroleumProductClass(self.db)

        # Create table for petroleum product
        petroleum_product_obj.create_table()

        # Store response data in db
        petroleum_product_obj.store_data_in_db(py_dict_data)

        # question 3
        print("\n List overall sale of each petroleam product by country: ")
        petroleum_product_obj.get_overall_sale_by_country()

        # question 4
        print("\n Average sales of each petroleum product in 2 years interval: ")
        petroleum_product_obj.get_avg_sale_for_two_year()

        # question number 5
        print("\n Least sales of each petroleum product: ")
        petroleum_product_obj.get_least_sale()

if __name__ == "__main__":
    main_obj = MainClass()
    main_obj.main()